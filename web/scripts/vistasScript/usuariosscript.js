var usuariosModelo = {};

var usuariosControlador = {
    servletRegistrarUsuario: function (datosUsu, resultado) {
        var configAjax = {
            url: 'usuario/nuevo',
            data: datosUsu,
            dataType: 'json',
            success: function (resultadoAjax) {
                resultado(resultadoAjax);
            },
            error: function () {
                alert('Error ');
            }
        };
        $.ajax(configAjax);
    },
    servletVerificarUsuario: function (datosUsu, resultado) {
        var configAjax = {
            url: 'usuario/login',
            data: datosUsu,
            dataType: 'json',
            success: function (resultadoAjax) {
                resultado(resultadoAjax);
            },
            error: function () {
                alert('Error ');
            }
        };
        $.ajax(configAjax);
    }


};

var usuariosVista = {
    iniciarvista: function () {
        //alert('Lo estamos logrando!!!!');
        $('#btnLimpiar').on('click', usuariosVista.limpiarFormulario);
        $('#btnRegistrar').on('click', usuariosVista.registrarUsuario);
    },
    iniciarvistaLogin: function () {
        if (sessionStorage.getItem('usuario')) {
            $(location).attr('href', 'aplicacion');
        }
        $('#btnLimpiar').on('click', usuariosVista.limpiarFormulario);
        $('#btnIngresar').on('click', usuariosVista.verificarUsuario);
    },
    limpiarFormulario: function () {
//        $('#txtNombres').val('');
//        $('#txtApellidos').val('');
//        $('#txtFechaNac').val('');
//        $('#txtCorreo').val('');
//        $('#txtPass').val('');
//        $('#txtPass2').val('');
        $('.com_frm').val('');
    },
    registrarUsuario: function () {
        usuariosModelo.datosUsu = {};
        usuariosModelo.datosUsu.nombre = $('#txtNombres').val();
        usuariosModelo.datosUsu.apellido = $('#txtApellidos').val();
        usuariosModelo.datosUsu.fechaNacimiento = $('#txtFechaNac').val();
        usuariosModelo.datosUsu.correo = $('#txtCorreo').val();
        usuariosModelo.datosUsu.clave = $('#txtPass').val();

        if ($('#txtPass').val() === $('#txtPass2').val()) {
            usuariosControlador.servletRegistrarUsuario(usuariosModelo.datosUsu, usuariosVista.respuestaServletRegistrar);
        } else {
            alert('Las contraseñas no coinciden');
        }
    },
    verificarUsuario: function () {
        usuariosModelo.datos = {};
        usuariosModelo.datos.correo = $('#txtCorreo').val();
        usuariosModelo.datos.password = $('#txtPass').val();

        if ($('#txtCorreo').val() == '' || $('#txtPass').val() == '') {
            alert('Datos Incompletos');
        } else {
            usuariosControlador.servletVerificarUsuario(usuariosModelo.datos, usuariosVista.respuestaServletVerificar);
        }

    },
    respuestaServletRegistrar: function (respuesta) {
        console.warn(respuesta);
        if (respuesta.codigo == 1) {
            alert('Usuario Creado Correctamente');
            $(location).attr('href', 'prin');
        } else {
            alert('Usuario NO Creado : ' + respuesta.mensaje);
        }
        usuariosVista.limpiarFormulario();
    },
    respuestaServletVerificar: function (respuesta) {
        console.warn(respuesta);
        if (respuesta.codigo == 1) {
            alert('Bienvenido');
            sessionStorage.setItem("usuario", JSON.stringify(respuesta.datos));
            console.warn(respuesta.datos);
            if (respuesta.datos.idUsuario == 1) {
                $(location).attr('href', 'administrador');
            } else {
                $(location).attr('href', 'aplicacion');
            }
        } else {
            alert('Error Login : ' + respuesta.mensaje);
        }
        usuariosVista.limpiarFormulario();
    }

};


