var adminModelo= {};

var adminControlador= {
    
    servletConsultarUsuarios:function (resultado){
        var configAjax = {
            url:'usuario/consultar',
            data:'',
            dataType:'json',
            success: function (resultadoAjax){
                resultado(resultadoAjax);
            },
            error:function (){
                alert('Error '); 
            }
        };
        $.ajax(configAjax);
    },
    
};

var adminVista = {
    
    iniciarvista :function (){
        adminControlador.servletConsultarUsuarios(adminVista.respuestaServletConsultar);
    },
    
    respuestaServletConsultar:function (respuesta){
        console.warn(respuesta);
        if(respuesta.codigo == 1){
            
            var columnas = [
                {
                    title:'Nombres',
                    data:'nombreUsuario'
                },
                {
                    title:'Apellidos',
                    data:'apellidoUsuario'
                },
                {
                    title:'Correo',
                    data:'correoUsuario'
                },
                {
                    title:'Eliminar',
                    defaultContent:'<input type="button" class="btnEliminar" value="Eliminar">'
                },
            ];
            adminModelo.tablaAdmin =  $('#tablaUsuarios').DataTable({
                data:respuesta.datos,
                columns:columnas
            });
            
            
            var combo = $('#listaUsu');
            
            $.each(respuesta.datos,function (i,dato){
                console.warn('Entro ' + dato);
                var nuevaOp = $('<option>');
                //nuevaOp.attr('value',dato.idusuario);
                nuevaOp.val(dato.idUsuario);
                nuevaOp.text(dato.nombreUsuario);
                combo.append(nuevaOp);
            })
            
            
            $('.btnEliminar').on('click',adminVista.eliminarUsuario);
            
            
        }else{
            alert('No se pueden consultar los usuarios ' + respuesta.mensaje);
        }
    },
    
    eliminarUsuario:function (){
        var datosUsuario = adminModelo.tablaAdmin.row($(this).parents('tr')).data();
        console.warn(datosUsuario);
        alert('Eliminando a : ' + datosUsuario.idUsuario);
    }
    
    
};

