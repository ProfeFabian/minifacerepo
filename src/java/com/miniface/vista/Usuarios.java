package com.miniface.vista;

import com.google.gson.Gson;
import com.miniface.controlador.ModuloUsuarios;
import com.miniface.modelo.vo.Usuario;
import com.miniface.utilidades.MinifaceException;
import com.miniface.utilidades.RespuestaMiniFace;
import com.miniface.utilidades.UtilidadesServlets;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Fabian
 */
@WebServlet(name = "Usuarios", urlPatterns = {"/usuario/nuevo", "/usuario/actualizar", "/usuario/eliminar", "/usuario/consultar", "/usuario/login", "/usuario/*"})
public class Usuarios extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String accion = UtilidadesServlets.accionServlet(request.getServletPath());
            RespuestaMiniFace respuesta = new RespuestaMiniFace();
            switch (accion) {
                case "nuevo":
                    respuesta = nuevoUsuario(request);

                    break;
                case "actualizar":
                    respuesta.setCodigo(0);
                    respuesta.setMensaje("Accion de Nuevo Actualizar");
                    break;
                case "eliminar":
                    respuesta.setCodigo(0);
                    respuesta.setMensaje("Accion de Nuevo Eliminar");
                    break;
                case "consultar":
                    respuesta = consultarTodosUsuarios();
                    break;
                case "login":
                    respuesta = verificarUsuario(request);
                    break;
                default:
                    respuesta.setCodigo(404);
                    respuesta.setMensaje("Accion No soportada");

            }

            Gson convertidorJson = new Gson();
            out.println(convertidorJson.toJson(respuesta));

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private RespuestaMiniFace nuevoUsuario(HttpServletRequest request) {
        String nombreUsuario = request.getParameter("nombre");
        String apellidoUsuario = request.getParameter("apellido");
        String fechaNacimientoUsuario = request.getParameter("fechaNacimiento");
        //String fotoUsuario;
        String correoUsuario = request.getParameter("correo");
        String claveUsuario = request.getParameter("clave");

        RespuestaMiniFace respuesta = new RespuestaMiniFace();

        if (nombreUsuario == null
                || apellidoUsuario == null
                || fechaNacimientoUsuario == null
                || correoUsuario == null
                || claveUsuario == null
                || fechaNacimientoUsuario == null
                || nombreUsuario.isEmpty()
                || apellidoUsuario.isEmpty()
                || fechaNacimientoUsuario.isEmpty()
                || correoUsuario.isEmpty()
                || claveUsuario.isEmpty()
                || fechaNacimientoUsuario.isEmpty()) {
            respuesta.setCodigo(101);
            respuesta.setMensaje("Datos Incompletos");
            return respuesta;
        }

        Usuario nuevoUsuario = new Usuario();
        nuevoUsuario.setNombreUsuario(nombreUsuario);
        nuevoUsuario.setApellidoUsuario(apellidoUsuario);
        nuevoUsuario.setClaveUsuario(claveUsuario);
        nuevoUsuario.setCorreoUsuario(correoUsuario);
        try {
            nuevoUsuario.setFechaNacimientoUsuario(UtilidadesServlets.convertirStringAUtilDate(fechaNacimientoUsuario));
            ModuloUsuarios modulo = new ModuloUsuarios();
            if (modulo.insertarUsuario(nuevoUsuario)) {
                respuesta.setCodigo(1);
                respuesta.setMensaje("Usuario Creado");
                respuesta.setDatos(nuevoUsuario);
                return respuesta;
            }
        } catch (MinifaceException ex) {
            respuesta.setCodigo(ex.getCodigo());
            respuesta.setMensaje(ex.getMensaje());
            return respuesta;
        }
        respuesta.setCodigo(300);
        respuesta.setMensaje("Error en algun lado");
        return respuesta;

    }

    private RespuestaMiniFace verificarUsuario(HttpServletRequest request) {
        String correo = request.getParameter("correo");
        String clave = request.getParameter("password");
        RespuestaMiniFace respuesta = new RespuestaMiniFace();
        if (correo == null || correo.isEmpty() || clave == null || clave.isEmpty()) {
            respuesta.setCodigo(-1);
            respuesta.setMensaje("Datos de Usuario Incompletos");
            return respuesta;
        }
        Usuario usu = new Usuario();
        usu.setCorreoUsuario(correo);
        usu.setClaveUsuario(clave);
        ModuloUsuarios modUsuarios = new ModuloUsuarios();
        try {
            modUsuarios.validarUsuario(usu);
            HttpSession sesionUsuario = request.getSession();
            sesionUsuario.setAttribute("usuariologin", usu);
            respuesta.setCodigo(1);
            respuesta.setMensaje("OK");
            respuesta.setDatos(usu);
            return respuesta;

        } catch (MinifaceException ex) {
            respuesta.setCodigo(ex.getCodigo());
            respuesta.setMensaje(ex.getMensaje());
            return respuesta;
        }

    }

    private RespuestaMiniFace consultarTodosUsuarios() {
        RespuestaMiniFace respuesta = new RespuestaMiniFace();
        try {
            ModuloUsuarios modUsuarios = new ModuloUsuarios();
            List<Usuario> listaUsuarios = modUsuarios.consultarTodo();
            respuesta.setCodigo(1);
            respuesta.setDatos(listaUsuarios);
            respuesta.setMensaje("Parametrice todo WEY!!!!");
            return respuesta;
        } catch (MinifaceException ex) {
            respuesta.setCodigo(ex.getCodigo());
            respuesta.setMensaje(ex.getMensaje());
            return respuesta;
        }
    }
    
    
    public void incertarPeticionPQR(HttpServletRequest request){
        Usuario prof = (Usuario)request.getSession().getAttribute("usuariologin");
    }

}
