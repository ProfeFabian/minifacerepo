package com.miniface.utilidades;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilidadesServlets {
    public static String accionServlet(String urlServlet){
        String[] arr = urlServlet.split("/");
        return arr[arr.length -1];
    }
    
    public static Date convertirStringAUtilDate(String fechaString) throws MinifaceException{
        try {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            return formato.parse(fechaString);
        } catch (ParseException ex) {
            throw new MinifaceException(202, "La fecha " + fechaString  + " no esta en el formao dd/MM/yyyy " + ex.getMessage() );
        }
    }
    
}
