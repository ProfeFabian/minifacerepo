package com.miniface.controlador;

import com.miniface.modelo.dao.Conexion;
import com.miniface.modelo.dao.UsuarioDAO;
import com.miniface.modelo.vo.Usuario;
import com.miniface.utilidades.MinifaceException;
import java.sql.SQLException;
import java.util.List;

public class ModuloUsuarios {

    Conexion conex;

    public ModuloUsuarios() {
        conex = new Conexion();
    }

    public boolean insertarUsuario(Usuario nuevoUsuario) throws MinifaceException {
        try {
            UsuarioDAO dao = new UsuarioDAO(conex.getCnn());
            boolean r = dao.insertarUsuario(nuevoUsuario);
            conex.getCnn().commit();
            return r;
        } catch (SQLException ex) {
            try {
                System.out.println(ex);
                conex.getCnn().rollback();
                throw new MinifaceException(-1, "Error al ingresar usuario");
            } catch (SQLException ex1) {
                throw new MinifaceException(-1, "Error al ingresar usuario");
            }
        } finally {
            try {
                conex.getCnn().close();
            } catch (SQLException ex) {
                //Logger.getLogger(ModuloUsuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void validarUsuario(Usuario usu) throws MinifaceException {
        try {
            UsuarioDAO dao = new UsuarioDAO(conex.getCnn());
            Usuario usuVerificado = dao.consultar(usu.getCorreoUsuario());
            if (usuVerificado == null) {
                throw new MinifaceException(-1, "No se encontro Usuario con los datos especificados");
            }
            if (!usu.getClaveUsuario().equals(usuVerificado.getClaveUsuario())) {
                throw new MinifaceException(-1, "Los datos de Usuario no son correctos");
            }
            usu.setApellidoUsuario(usuVerificado.getApellidoUsuario());
            usu.setClaveUsuario(usuVerificado.getClaveUsuario());
            usu.setCorreoUsuario(usuVerificado.getCorreoUsuario());
            usu.setFechaNacimientoUsuario(usuVerificado.getFechaNacimientoUsuario());
            usu.setFotoUsuario(usuVerificado.getFotoUsuario());
            usu.setIdUsuario(usuVerificado.getIdUsuario());
            usu.setNombreUsuario(usuVerificado.getNombreUsuario());
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new MinifaceException(-1, "Error en Login Usuario");
        } finally {
            try {
                conex.getCnn().close();
            } catch (SQLException ex) {
                //Logger.getLogger(ModuloUsuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public List<Usuario> consultarTodo() throws MinifaceException {
        try {
            UsuarioDAO dao = new UsuarioDAO(conex.getCnn());
            List<Usuario> listaUsuarios = dao.consultar();
            return listaUsuarios;
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new MinifaceException(-1, "Error Consultando Usuarios " + ex.getMessage());
        }finally{
             try {
                conex.getCnn().close();
            } catch (SQLException ex) {
                //Logger.getLogger(ModuloUsuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
