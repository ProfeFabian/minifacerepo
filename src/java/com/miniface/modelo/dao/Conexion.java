
package com.miniface.modelo.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class Conexion {
    private Connection cnn;
    
    public Conexion(){
        try {
            DataSource ds = InitialContext.doLookup("jdbc/miniface");
            cnn = ds.getConnection();
            cnn.setAutoCommit(false);
        } catch (NamingException | SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getCnn() {
        return cnn;
    }

    public void setCnn(Connection cnn) {
        this.cnn = cnn;
    }
    
}
