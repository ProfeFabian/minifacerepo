/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.miniface.modelo.dao;

import com.miniface.modelo.vo.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fabian
 */
public class UsuarioDAO {

    Connection cnn;

    public UsuarioDAO(Connection cnn) {
        this.cnn = cnn;
    }

    public boolean insertarUsuario(Usuario usuario) throws SQLException {
        String sql = "INSERT INTO usuario( nombre_usuario, apellido_usuario, fecha_nacimiento_usuario, foto_usuario, correo_usuario, clave_usuario)"
                + " VALUES (?,?,?,?,?,?)";
        PreparedStatement sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        sentencia.setString(1, usuario.getNombreUsuario());
        sentencia.setString(2, usuario.getApellidoUsuario());
        sentencia.setDate(3, new java.sql.Date(usuario.getFechaNacimientoUsuario().getTime()));
        sentencia.setString(4, usuario.getFotoUsuario());
        sentencia.setString(5, usuario.getCorreoUsuario());
        sentencia.setString(6, usuario.getClaveUsuario());

        sentencia.executeUpdate();
        ResultSet llave = sentencia.getGeneratedKeys();
        if (llave.next()) {
            usuario.setIdUsuario(llave.getInt(1));
            return true;
        }
        return false;
    }

    public Usuario consultar(String correoUsuario) throws SQLException {

        Usuario usuRetorno = null;

        String sql = "SELECT * FROM usuario WHERE correo_usuario = ?";

        PreparedStatement sentencia = cnn.prepareStatement(sql);
        sentencia.setString(1, correoUsuario);
        ResultSet resultado = sentencia.executeQuery();
        if (resultado.next()) {
            usuRetorno = new Usuario();
            usuRetorno.setIdUsuario(resultado.getInt("id_usuario"));
            usuRetorno.setNombreUsuario(resultado.getString("nombre_usuario"));
            usuRetorno.setApellidoUsuario(resultado.getString("apellido_usuario"));
            usuRetorno.setCorreoUsuario(resultado.getString("correo_usuario"));
            usuRetorno.setFechaNacimientoUsuario(resultado.getDate("fecha_nacimiento_usuario"));
            usuRetorno.setClaveUsuario(resultado.getString("clave_usuario"));
            usuRetorno.setFotoUsuario(resultado.getString("foto_usuario"));
        }
        return usuRetorno;
    }

    public List<Usuario> consultar() throws SQLException {

        String sql = "SELECT * FROM usuario";
        List<Usuario> listaUsuarios = new ArrayList<>();
        PreparedStatement sentencia = cnn.prepareStatement(sql);
        ResultSet resultado = sentencia.executeQuery();
        while (resultado.next()) {
            Usuario usuTemporal = new Usuario();
            usuTemporal.setIdUsuario(resultado.getInt("id_usuario"));
            usuTemporal.setNombreUsuario(resultado.getString("nombre_usuario"));
            usuTemporal.setApellidoUsuario(resultado.getString("apellido_usuario"));
            usuTemporal.setCorreoUsuario(resultado.getString("correo_usuario"));
            usuTemporal.setFechaNacimientoUsuario(resultado.getDate("fecha_nacimiento_usuario"));
            usuTemporal.setClaveUsuario(resultado.getString("clave_usuario"));
            usuTemporal.setFotoUsuario(resultado.getString("foto_usuario"));
            listaUsuarios.add(usuTemporal);
        }
        return listaUsuarios;
    }
}